<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class T_balaskomentar extends Model
{
    protected $fillable = ['t_komentar_id','isi','tgl_komentar','nama'];

    public function t_komentars()
    {
        return $this->belongsTo('App\T_komentar','t_komentar_id');
    }
}

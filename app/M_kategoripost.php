<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_kategoripost extends Model
{
    protected $fillable = ['nama'];

    public function m_postfronts()
    {
        return $this->hasMany('App\M_post')->orderBy('id','desc')->limit('5');
    }

    public function m_posts()
    {
        return $this->hasMany('App\M_post')->orderBy('id','desc');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_websetting extends Model
{
    protected $fillable = ['nama','isi','nilai','key'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class T_register extends Model
{
    protected $fillable = ['kode','nama','no_unas','kelamin','alamat','sekolah_asal','jenis_kartu','nomer_kartu','nilai_bahasindonesia','nilai_matematika','nilai_ipa','tgl_daftar'];

    public function t_prestasiregisters()
    {
        return $this->hasMany('App\T_prestasiregister')->orderBy('id','asc');
    }
}

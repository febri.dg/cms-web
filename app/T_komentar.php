<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class T_komentar extends Model
{
    protected $fillable = ['m_post_id','isi','tgl_komentar','nama'];

    public function m_posts()
    {
        return $this->belongsTo('App\M_post','m_post_id');
    }
}

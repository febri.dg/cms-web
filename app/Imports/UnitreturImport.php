<?php

namespace App\Imports;

use App\T_stock;

use App\T_rekapstock;

use App\T_produkstock;

use App\M_user;

use App\M_produk;

use App\T_detailreturpenjualan;

use App\T_detailunitretur;

use Maatwebsite\Excel\Concerns\ToModel;

class UnitreturImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public $idtoken;
    public $tgl;

    public function __construct($idtoken)
    {
        $this->idtoken = $idtoken;
        $this->tgl = date('Y-m-d');

        $detailunitretur = T_detailunitretur::where('tmptokenid',$this->idtoken)->orderBy('id')->get();

        foreach ($detailunitretur as $dp) {
            $editdetail = T_detailunitretur::find($dp->id);
            $editdetail->nomer_order = null;
            $editdetail->nomer_serial = null;
            $editdetail->save();
        }
    }

    public function model(array $row)
    {
        $editdetail = T_detailunitretur::where('tmptokenid',$this->idtoken)->where('part_id',trim($row[0]))->whereNull('nomer_order')->whereNull('nomer_serial')->first();
        
        $editdetail->nomer_order = trim($row[1]);
        $editdetail->nomer_serial = trim($row[2]);
        $editdetail->save();
    }
}
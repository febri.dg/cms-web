<?php

namespace App\Imports;

use App\M_produk;

use App\T_detailstockopname;
use Maatwebsite\Excel\Concerns\ToModel;

class StockopnameImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public $idtoken;
    public $tgl;

    public function __construct($idtoken)
    {
        $this->idtoken = $idtoken;
        $this->tgl = date('Y-m-d');

        $detailstockopname = T_detailstockopname::where('tmptokenid',$this->idtoken)->orderBy('id')->get();

        foreach ($detailstockopname as $dp) {
            $editdetail = T_detailstockopname::find($dp->id);
            $editdetail->nomer_order = null;
            $editdetail->nomer_serial = null;
            $editdetail->save();
        }
    }

    public function model(array $row)
    {
        $produk = M_produk::where('part_id',trim($row[0]))->first();
        $editdetail = T_detailstockopname::where('tmptokenid',$this->idtoken)->where('m_produk_id',$produk->id)->whereNull('nomer_order')->whereNull('nomer_serial')->first();
        
        $editdetail->nomer_order = trim($row[1]);
        $editdetail->nomer_serial = trim($row[2]);
        $editdetail->save();
    }
}

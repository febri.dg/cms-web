<?php

namespace App\Imports;

use App\T_detailunitstagingkeluar;
use Maatwebsite\Excel\Concerns\ToModel;

class UnitstagingkeluarImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public $idtoken;
    public $tgl;

    public function __construct($idtoken)
    {
        $this->idtoken = $idtoken;
        $this->tgl = date('Y-m-d');

        $detailunitstagingkeluar = T_detailunitstagingkeluar::where('tmptokenid',$this->idtoken)->orderBy('id')->get();

        foreach ($detailunitstagingkeluar as $dp) {
            $editdetail = T_detailunitstagingkeluar::find($dp->id);
            $editdetail->nomer_serial = null;
            $editdetail->save();
        }
    }

    public function model(array $row)
    {
        $editdetail = T_detailunitstagingkeluar::where('tmptokenid',$this->idtoken)->where('part_id',trim($row[0]))->whereNull('nomer_serial')->first();

        $editdetail->nomer_serial = trim($row[1]);
        $editdetail->save();
    }
}

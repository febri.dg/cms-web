<?php

namespace App\Imports;

use App\T_stock;

use App\T_rekapstock;

use App\T_produkstock;

use App\M_user;

use App\T_detailorderpembelian;

use App\M_produk;

use App\T_detailpenerimaanbarang;

use Maatwebsite\Excel\Concerns\ToModel;

class PenerimaanbarangImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public $idtoken;
    public $tgl;

    public function __construct($idtoken)
    {
        $this->idtoken = $idtoken;
        $this->tgl = date('Y-m-d');
    }

    public function model(array $row)
    {
        $detailpenerimaanbarang = T_detailpenerimaanbarang::where('tmptokenid',$this->idtoken)->orderBy('id')->get();

        foreach ($detailpenerimaanbarang as $dk) {
            $editdetail = T_detailpenerimaanbarang::find($dk->id);
            $editdetail->nomer_order = trim($row[1]);
            $editdetail->nomer_serial = trim($row[2]);
            $editdetail->save();
        }
    }
}
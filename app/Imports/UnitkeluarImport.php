<?php

namespace App\Imports;

use App\T_stock;

use App\T_rekapstock;

use App\T_produkstock;

use App\M_user;

use App\M_produk;

use App\T_detailorderpenjualan;

use App\T_detailunitkeluar;

use Maatwebsite\Excel\Concerns\ToModel;

class UnitkeluarImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public $idtoken;
    public $tgl;

    public function __construct($idtoken)
    {
        $this->idtoken = $idtoken;
        $this->tgl = date('Y-m-d');

        $detailunitkeluar = T_detailunitkeluar::where('tmptokenid',$this->idtoken)->orderBy('id')->get();

        foreach ($detailunitkeluar as $dp) {
            $editdetail = T_detailunitkeluar::find($dp->id);
            $editdetail->nomer_order = null;
            $editdetail->nomer_serial = null;
            $editdetail->save();
        }
    }

    public function model(array $row)
    {
        $editdetail = T_detailunitkeluar::where('tmptokenid',$this->idtoken)->where('part_id',trim($row[0]))->whereNull('nomer_order')->whereNull('nomer_serial')->first();

        $editdetail->nomer_order = trim($row[1]);
        $editdetail->nomer_serial = trim($row[2]);
        $editdetail->save();
    }
}
<?php
namespace App\Imports;

use App\M_user;
use App\T_detailorderpembelian;
use App\M_produk;
use App\T_detailpenerimaanbarang;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PenerimaanbarangImport implements ToCollection
{
    public $idtoken;

    public function __construct($idtoken)
    {
        $this->idtoken = $idtoken;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $detailorderpembelian = T_detailorderpembelian::where('part_id',$row[1])->first();

            $penerimaanbarang_id = 0;

            T_detailpenerimaanbarang::create([
                't_penerimaanbarang_id' => $penerimaanbarang_id,
                'nomer_po' => $row[0],
                'part_id' => $row[1],
                'nama_produk' => $row[2],
                'nomer_order' => $row[3],
                'nomer_serial' => $row[4],
                'm_produk_id' => $detailorderpembelian->m_produk_id,
                'tmptokenid' => $this->idtoken,
                't_detailorderpembelian_id' => $detailorderpembelian->id,
                'jml_produk' => $detailorderpembelian->jml_produk
            ]);
        }
    }
}
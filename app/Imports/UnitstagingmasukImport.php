<?php

namespace App\Imports;

use App\T_detailunitstagingmasuk;
use Maatwebsite\Excel\Concerns\ToModel;

class UnitstagingmasukImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public $idtoken;
    public $tgl;

    public function __construct($idtoken)
    {
        $this->idtoken = $idtoken;
        $this->tgl = date('Y-m-d');

        $detailunitstagingmasuk = T_detailunitstagingmasuk::where('tmptokenid',$this->idtoken)->orderBy('id')->get();

        foreach ($detailunitstagingmasuk as $dp) {
            $editdetail = T_detailunitstagingmasuk::find($dp->id);
            $editdetail->nomer_serial = null;
            $editdetail->save();
        }
    }

    public function model(array $row)
    {
        $editdetail = T_detailunitstagingmasuk::where('tmptokenid',$this->idtoken)->where('part_id',trim($row[0]))->whereNull('nomer_serial')->first();

        $editdetail->nomer_serial = trim($row[1]);
        $editdetail->save();
    }
}

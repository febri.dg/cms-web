<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\M_album;

class viewalbum extends Controller
{
  public function listalbum()
  {
    $album = M_album::all();

    return view('admin.album.listalbum',['album' => $album]);
  }

  public function tambahalbum()
  {
    return view('admin.album.tambahalbum');
  }

  public function aksitambahalbum(request $request)
  {
    M_album::create([
        'nama' => $request->nama
    ]);

    return redirect( env('APP_URL').'/admin/album/tambahalbum')->with('statusalbum','Album baru berhasil ditambahkan');
  }

  public function editalbum($id)
  {
    $album = M_album::find($id);

    return view('admin.album.editalbum',['album' => $album]);
  }

  public function aksieditalbum(request $request,$id)
  {
    $album = M_album::find($id);
    $album->nama = $request->nama;
    $album->save();

    return redirect( env('APP_URL').'/admin/album/editalbum/'.$id)->with('statusalbum','Album berhasil diedit');
  }
}

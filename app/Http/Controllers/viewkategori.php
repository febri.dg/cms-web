<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\M_kategoripost;

class viewkategori extends Controller
{
    public function listkategori()
    {
      $kategoripost = M_kategoripost::all();

      return view('admin.kategoripost.listkategori',['kategoripost' => $kategoripost]);
    }

    public function tambahkategori()
    {
      return view('admin.kategoripost.tambahkategori');
    }

    public function aksitambahkategori(request $request)
    {
      M_kategoripost::create([
          'nama' => $request->nama
      ]);

      return redirect( env('APP_URL').'/admin/kategoripost/tambahkategori')->with('statuskategoripost','Kategori Post baru berhasil ditambahkan');
    }

    public function editkategori($id)
    {
      $kategoripost = M_kategoripost::find($id);

      return view('admin.kategoripost.editkategori',['kategoripost' => $kategoripost]);
    }

    public function aksieditkategori(request $request,$id)
    {
      $kategoripost = M_kategoripost::find($id);
      $kategoripost->nama = $request->nama;
      $kategoripost->save();

      return redirect( env('APP_URL').'/admin/kategoripost/editkategori/'.$id)->with('statuskategoripost','Kategori Post berhasil diedit');
    }
}

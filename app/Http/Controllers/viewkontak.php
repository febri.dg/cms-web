<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;

use App\M_webmenu;

use App\M_websubmenu;

class viewkontak extends Controller
{
  public function kontak()
  {
    $webmenu = M_webmenu::orderBy('orders')->get();

    return view('web.kontak',['webmenu' => $webmenu]);
  }
}

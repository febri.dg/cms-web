<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;

class viewadminhome extends Controller
{
    public function home()
    {
        if (Session::has('username')){
            //$menu = Menu::all();

            return view('admin.home.home');     
        }else{
            return view('login');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;

use App\M_webmenu;

use App\M_websubmenu;

use App\M_websetting;

class viewprofil extends Controller
{
    public function profil()
    {
      $webmenu = M_webmenu::orderBy('orders')->get();

      $profil = M_websetting::find(6);

      return view('web.profil',['webmenu' => $webmenu,'profil' => $profil]);
    }
}

<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;

use App\M_webmenu;

use App\M_websubmenu;

use App\M_post;

use App\M_kategoripost;

use App\M_websetting;

class viewwebpost extends Controller
{
  public function post($tag)
  {
    $webmenu = M_webmenu::orderBy('orders')->get();
    $post = M_post::where('tag',$tag)->first();
    $postterbaru = M_post::orderBy('id','desc')->limit(10)->get();
    $profil = M_websetting::find(6);

    return view('web.post',['webmenu' => $webmenu,'post' => $post,'postterbaru' => $postterbaru,'profil' => $profil]);
  }
}

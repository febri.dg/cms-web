<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Support\Facades\File;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\M_user;

class aksilogin extends Controller
{
    public function login(Request $request)
    {
        $password = md5($request->password);

        $m_user = M_user::where('username',$request->username)
                                ->where('password',$password);

        $usercek = $m_user->get();
        $cek = count($usercek);

        $userlogin = $m_user->first();

        if ($cek>0) {
            Session::put('username',$userlogin->username);
            Session::put('iduser',$userlogin->id);
            Session::put('namauser',$userlogin->nama);
            Session::put('otoritas',$userlogin->m_otoriti_id);

            return redirect(env('APP_URL').'/admin');
        }else{
            return redirect(env('APP_URL').'/admin')->with('statuslogin','Username dan Password tidak sesuai');
        }

    }

    public function logout()
    {
        Session::flush();

        return redirect(env('APP_URL').'/admin');
    }

    public function modul($id)
    {
        Session::put('idmodul',$id);

        return redirect(env('APP_URL').'/');
    }

    public function gantipassword()
    {
        $iduser = Session::get('iduser');

        return view('utility.pegawai.gantipassword',['iduser' => $iduser]);
    }

    public function aksigantipassword($id,Request $request)
    {
        if ($request->password1<>$request->password2) {
            return redirect( env('APP_URL').'/gantipassword')->with('statusgantipassword','Password tidak sesuai');
        }else{
            $password = md5($request->password1);

            $pegawai = M_user::find($id);
            $pegawai->password = $password;
            $pegawai->save();

            return redirect( env('APP_URL').'/gantipassword')->with('statusgantipassword','Password berhasil diedit');
        }
    }
}

<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\M_websetting;

class viewsetting extends Controller
{
    public function setting()
    {
        $nama = M_websetting::where('nama','nama')->first();
        $alamat = M_websetting::where('nama','alamat')->first();
        $kota = M_websetting::where('nama','kota')->first();
        $telp = M_websetting::where('nama','telp')->first();
        $email = M_websetting::where('nama','email')->first();
        $profil = M_websetting::where('nama','profil')->first();

        return view('admin.setting.setting',['nama' => $nama,'alamat' => $alamat,'kota' => $kota,'telp' => $telp,'email' => $email,'profil' => $profil]);
    }

    public function aksieditsetting(Request $request)
    {
      $editnama = M_websetting::where('nama','nama')->first();
      $editnama->nilai = $request->nama;
      $editnama->save();

      $editalamat = M_websetting::where('nama','alamat')->first();
      $editalamat->nilai = $request->alamat;
      $editalamat->save();

      $editkota = M_websetting::where('nama','kota')->first();
      $editkota->nilai = $request->kota;
      $editkota->save();

      $edittelp = M_websetting::where('nama','telp')->first();
      $edittelp->nilai = $request->telp;
      $edittelp->save();

      $editemail = M_websetting::where('nama','email')->first();
      $editemail->nilai = $request->email;
      $editemail->save();

      $editprofil = M_websetting::where('nama','profil')->first();
      $editprofil->isi = $request->profil;
      $editprofil->save();

      $nama = M_websetting::where('nama','nama')->first();
      $alamat = M_websetting::where('nama','alamat')->first();
      $kota = M_websetting::where('nama','kota')->first();
      $telp = M_websetting::where('nama','telp')->first();
      $email = M_websetting::where('nama','email')->first();
      $profil = M_websetting::where('nama','profil')->first();

      return view('admin.setting.setting',['nama' => $nama,'alamat' => $alamat,'kota' => $kota,'telp' => $telp,'email' => $email,'profil' => $profil]);
    }
}

<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\M_otoriti;

use App\M_user;

class viewuser extends Controller
{
  public function listuser()
  {
      $user = M_user::where('username','<>','sysadmin')->get();

      return view('admin.user.listuser',['user' => $user]);
  }

  public function tambahuser()
  {
      $otoriti = M_otoriti::all();

      return view('admin.user.tambahuser',['otoriti' => $otoriti]);
  }

  public function aksitambahuser(Request $request)
  {
    if ($request->password1<>$request->password2) {
        return redirect( env('APP_URL').'/admin/user/tambahuser')->with('statususer','Password tidak sesuai');
    }else{
        $password = md5($request->password1);
        $otoriti = M_otoriti::find($request->otoriti);

        M_user::create([
            'username' => $request->username,
            'password' => $password,
            'm_otoriti_id' => $request->otoritas,
            'nama' => $request->nama
        ]);
    }

    return redirect( env('APP_URL').'/admin/user/tambahuser')->with('statususer','Data User berhasil ditambahkan');
  }

  public function edituser($id)
  {
      $user = M_user::find($id);

      $otoriti = M_otoriti::all();

      return view('admin.user.edituser',['user' => $user,'otoriti' => $otoriti]);
  }

  public function aksiedituser($id,Request $request)
  {
    if (empty($request->password1) && empty($request->password2)) {
        $otoriti = M_otoriti::find($request->otoritas);

        $user = M_user::find($id);
        $user->nama = $request->nama;
        $user->m_otoriti_id = $request->otoritas;
        $user->save();

        return redirect( env('APP_URL').'/admin/user/edituser/'.$id)->with('statususer','Data User berhasil diedit');
    }else{
        if ($request->password1<>$request->password2) {
            return redirect( env('APP_URL').'/admin/user/edituser/'.$id)->with('statususer','Password tidak sesuai');
        }else{
            $password = md5($request->password1);
            $otoriti = M_otoriti::find($request->otoritas);

            $user = M_user::find($id);
            $user->nama = $request->nama;
            $user->m_otoriti_id = $request->otoritas;
            $user->password = $password;
            $user->save();

            return redirect( env('APP_URL').'/admin/user/edituser/'.$id)->with('statususer','Data User berhasil diedit');
        }
    }
  }

  public function deleteuser($id)
  {
      $user = M_user::find($id);
      $user->delete();

      $user = M_user::where('username','<>','sysadmin')->get();

      return view('admin.user.listuser',['user' => $user]);
  }
}

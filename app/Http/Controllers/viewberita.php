<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;

use App\M_webmenu;

use App\M_websubmenu;

use App\M_post;

use App\M_kategoripost;

use App\M_websetting;

class viewberita extends Controller
{
  public function berita()
  {
    $webmenu = M_webmenu::orderBy('orders')->get();
    $kategoriberita = M_kategoripost::find(1);
    $postterbaru = M_post::orderBy('id','desc')->limit(10)->get();
    $profil = M_websetting::find(6);

    return view('web.berita',['webmenu' => $webmenu,'kategoriberita' => $kategoriberita,'postterbaru' => $postterbaru,'profil' => $profil]);
  }
}

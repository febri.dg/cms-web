<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\M_menu;

use App\M_submenu;

use App\M_otoriti;

use App\M_detailotoriti;

class viewotoritas extends Controller
{
    public function listotoritas()
    {
        $otoriti = M_otoriti::orderBy('id','desc')->get();
        $detailotoriti = M_detailotoriti::select('m_otoriti_id', DB::raw("sum(m_otoriti_id)"))->groupBy('m_otoriti_id')->orderBy('m_otoriti_id','desc')->get();

        return view('admin.otoritas.listotoritas',['otoriti' => $otoriti,'detailotoriti' => $detailotoriti]);
    }

    public function tambahotoritas()
    {
        return view('admin.otoritas.tambahotoritas');
    }

    public function aksitambahotoritas(Request $request)
    {
      M_otoriti::create([
          'nama' => $request->nama
      ]);

      return redirect( env('APP_URL').'/admin/otoritas/tambahotoritas')->with('statusotoritas','Otoritas baru berhasil ditambahkan');
    }

    public function editotoritas($id)
    {
        $otoritas = M_otoriti::find($id);

        return view('admin.otoritas.editotoritas',['otoritas' => $otoritas]);
    }

    public function aksieditotoritas($id,Request $request)
    {
      $otoritas = M_otoriti::find($id);
      $otoritas->nama = $request->nama;
      $otoritas->save();

      return redirect( env('APP_URL').'/admin/otoritas/editotoritas/'.$id)->with('statusotoritas','Otoritas berhasil diedit');
    }

    public function tambahdetailotoritas()
    {
        $otoritas = M_otoriti::orderBy('id','desc')->get();
        $menu = M_menu::all();

        return view('admin.otoritas.tambahdetailotoritas',['otoritas' => $otoritas,'menu' => $menu]);
    }

    public function aksitambahdetailotoritas(Request $request)
    {
      $menu = M_menu::all();

      foreach ($menu as $mn) {
          foreach ($mn->m_submenus as $smn) {
              $idmenu = $mn->id;
              $idsubmenu = $smn->id;
              if ($request->input("sm$idmenu$idsubmenu")==$smn->id) {
                  M_detailotoriti::create([
                      'm_otoriti_id' => $request->otoritas,
                      'm_menu_id' => $mn->id,
                      'm_submenu_id' => $smn->id,
                      'ordermenu' => $mn->orders,
                      'ordersubmenu' => $smn->orders
                  ]);
              }
          }
      }

      return redirect( env('APP_URL').'/admin/otoritas/tambahdetailotoritas')->with('statusdetailotoritas','Detail otoritas berhasil ditambahkan');
    }

    public function editdetailotoritas($id)
    {
        $otoritas = M_otoriti::where('id',$id)->first();
        $menu = M_menu::all();

        return view('admin.otoritas.editdetailotoritas',['otoritas' => $otoritas,'menu' => $menu]);
    }

    public function aksieditdetailotoritas($id,Request $request)
    {
        $detailotoritas = M_detailotoriti::where('m_otoriti_id',$id);
        $detailotoritas->delete();

        $menu = M_menu::all();

        foreach ($menu as $mn) {
            foreach ($mn->m_submenus as $smn) {
                $idmenu = $mn->id;
                $idsubmenu = $smn->id;
                if ($request->input("sm$idmenu$idsubmenu")==$smn->id) {
                    M_detailotoriti::create([
                        'm_otoriti_id' => $request->otoritas,
                        'm_menu_id' => $mn->id,
                        'm_submenu_id' => $smn->id,
                        'ordermenu' => $mn->orders,
                        'ordersubmenu' => $smn->orders
                    ]);
                }
            }
        }

        return redirect( env('APP_URL').'/admin/otoritas/editdetailotoritas/'.$id)->with('statusdetailotoritas','Detail otoritas berhasil di edit');
    }

    public function deleteotoritas($id)
    {
        $otoritas = M_otoriti::find($id);
        $otoritas->delete();

        $detailotoritas = M_detailotoriti::where('m_otoriti_id',$id);
        $detailotoritas->delete();

        $otoriti = M_otoriti::orderBy('id','desc')->get();
        $detailotoriti = M_detailotoriti::select('m_otoriti_id', DB::raw("sum(m_otoriti_id)"))->groupBy('m_otoriti_id')->orderBy('m_otoriti_id','desc')->get();

        return view('admin.otoritas.listotoritas',['otoriti' => $otoriti,'detailotoriti' => $detailotoriti]);
    }

    public function deletedetailotoritas($id)
    {
        $detailotoritas = M_detailotoriti::where('m_otoriti_id',$id);
        $detailotoritas->delete();

        $otoriti = M_otoriti::orderBy('id','desc')->get();
        $detailotoriti = M_detailotoriti::select('m_otoriti_id', DB::raw("sum(m_otoriti_id)"))->groupBy('m_otoriti_id')->orderBy('m_otoriti_id','desc')->get();

        return view('admin.otoritas.listotoritas',['otoriti' => $otoriti,'detailotoriti' => $detailotoriti]);
    }
}

<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;

use App\M_webmenu;

use App\M_websubmenu;

use App\M_post;

use App\M_kategoripost;

use App\M_websetting;

class viewhome extends Controller
{
    public function home()
    {
      $webmenu = M_webmenu::orderBy('orders')->get();
      $postslide = M_post::where('homepage','Y')->orderBy('id','desc')->get();
      $kategoriberita = M_kategoripost::find(1);
      $kategoriartikel = M_kategoripost::find(2);
      $postterbaru = M_post::orderBy('id','desc')->limit(10)->get();
      $profil = M_websetting::find(6);

      return view('web.home',['webmenu' => $webmenu,'postslide' => $postslide,'kategoriberita' => $kategoriberita,'kategoriartikel' => $kategoriartikel,'postterbaru' => $postterbaru,'profil' => $profil]);
    }
}

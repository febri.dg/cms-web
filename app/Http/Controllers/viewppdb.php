<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\T_register;
use App\T_prestasiregister;

class viewppdb extends Controller
{
    public function listppdb()
    {
      $register = T_register::all();

      return view('admin.ppdb.listppdb',['register' => $register]);
    }

    public function editppdb($id)
    {
      $register = T_register::find($id);

      return view('admin.ppdb.editppdb',['register' => $register]);
    }
}

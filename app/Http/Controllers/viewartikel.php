<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;

use App\M_webmenu;

use App\M_websubmenu;

use App\M_post;

use App\M_kategoripost;

use App\M_websetting;

class viewartikel extends Controller
{
  public function artikel()
  {
    $webmenu = M_webmenu::orderBy('orders')->get();
    $kategoriartikel = M_kategoripost::find(2);
    $postterbaru = M_post::orderBy('id','desc')->limit(10)->get();
    $profil = M_websetting::find(6);

    return view('web.artikel',['webmenu' => $webmenu,'kategoriartikel' => $kategoriartikel,'postterbaru' => $postterbaru,'profil' => $profil]);
  }
}

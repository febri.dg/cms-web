<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\M_post;

use App\M_kategoripost;

class viewpost extends Controller
{
    public function listpost()
    {
      $post = M_post::all();

      return view('admin.post.listpost',['post' => $post]);
    }

    public function tambahpost()
    {
      $kategoripost = M_kategoripost::all();

      return view('admin.post.tambahpost',['kategoripost' => $kategoripost]);
    }

    public function aksitambahpost(request $request)
    {
      $file = $request->file('gambar');

      $nama_file = rand().$file->getClientOriginalName();

      $file->move('public/galeri',$nama_file);

      $tag = str_replace(' ', '-', $request->judul);

      $tgl = date('Y-m-d');

      M_post::create([
          'm_kategoripost_id' => $request->kategori,
          'judul' => $request->judul,
          'isi' => $request->isi,
          'gambar' => $nama_file,
          'key' => $request->key,
          'tag' => $tag,
          'tgl_post' => $tgl,
          'post' => $request->aktif,
          'homepage' => $request->homepage
      ]);

      return redirect( env('APP_URL').'/admin/post/tambahpost')->with('statuspost','Post baru berhasil ditambahkan');
    }

    public function editpost($id)
    {
      $post = M_post::find($id);
      $kategoripost = M_kategoripost::all();

      return view('admin.post.editpost',['post' => $post,'kategoripost' => $kategoripost]);
    }

    public function aksieditpost($id,Request $request)
    {
      $file = $request->file('gambar');

      $tag = str_replace(' ', '-', $request->judul);

      $tgl = date('Y-m-d');

      if (isset($file)) {
        $nama_file = rand().$file->getClientOriginalName();

        $file->move('public/galeri',$nama_file);

        $post = M_post::find($id);
        $post->m_kategoripost_id = $request->kategori;
        $post->judul = $request->judul;
        $post->isi = $request->isi;
        $post->key = $request->key;
        $post->tag = $tag;
        $post->tgl_post = $tgl;
        $post->post = $request->aktif;
        $post->homepage = $request->homepage;
        $post->gambar = $nama_file;
        $post->save();
      }else {
        $post = M_post::find($id);
        $post->m_kategoripost_id = $request->kategori;
        $post->judul = $request->judul;
        $post->isi = $request->isi;
        $post->key = $request->key;
        $post->tag = $tag;
        $post->tgl_post = $tgl;
        $post->post = $request->aktif;
        $post->homepage = $request->homepage;
        $post->save();
      }

      return redirect( env('APP_URL').'/admin/post/editpost/'.$id)->with('statuspost','Post berhasil diedit');
    }

    public function deletepost($id)
    {
        $post = M_post::find($id);
        $post->delete();

        $post = M_post::all();

        return view('admin.post.listpost',['post' => $post]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class T_prestasiregister extends Model
{
    protected $fillable = ['t_register_id','nama_kejuaraan','tingkat'];

    public function t_registers()
    {
        return $this->belongsTo('App\T_register','t_register_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_user extends Model
{
    protected $fillable = ['username','password','m_otoriti_id','nama','alamat','telp','gambar'];

    public function m_otoritis()
    {
        return $this->belongsTo('App\M_otoriti','m_otoriti_id');
    }
}

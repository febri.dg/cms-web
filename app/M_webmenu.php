<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_webmenu extends Model
{
    public function m_websubmenus()
    {
        return $this->hasMany('App\M_websubmenu')->orderBy('orders','asc');
    }
}

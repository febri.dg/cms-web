<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_otoriti extends Model
{
    protected $fillable = ['nama'];
}

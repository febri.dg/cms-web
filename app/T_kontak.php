<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class T_kontak extends Model
{
    protected $fillable = ['nama','alamat','email','subjek','isi','tgl'];
}

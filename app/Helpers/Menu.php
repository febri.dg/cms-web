<?php
namespace App\Helpers;

use DB;

use App\M_detailotoriti;
use App\M_otoriti;
use App\M_menu;
use App\M_submenu;
use App\M_webmenu;
use App\M_websubmenu;

class Menu
{
    public static function get_menu($otoriti,$userlogin)
    {
        $menu = M_detailotoriti::where('m_otoriti_id',$otoriti)->select('m_menu_id')->groupBy('m_menu_id','ordermenu')->orderBy('ordermenu')->get();

        return ($menu);
    }

    public static function get_submenu($menu,$otoriti,$userlogin)
    {
        $submenu = M_detailotoriti::where('m_otoriti_id',$otoriti)->where('m_menu_id',$menu)->orderBy('ordersubmenu')->get();

        return ($submenu);
    }

    public function get_webmenu()
    {
      $webmenu = M_webmenu::orderBy('orders')->get();

      return ($webmenu);
    }

    public function get_websubmenu($menu)
    {
      $websubmenu = M_websubmenu::where('m_webmenu_id',$menu)->orderBy('orders')->get();

      return ($websubmenu);
    }
}

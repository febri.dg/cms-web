<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_post extends Model
{
    protected $fillable = ['m_kategoripost_id','judul','isi','gambar','key','tag','tgl_post','post','homepage'];

    public function m_kategoriposts()
    {
        return $this->belongsTo('App\M_kategoripost','m_kategoripost_id');
    }
}

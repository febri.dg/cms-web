<?php

namespace App\Exports;

use App\T_detailorderpembelian;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class RekappembelianExport implements WithColumnFormatting, FromView
{
    public $tgl1;
    public $tgl2;

    public function __construct($tgl1,$tgl2)
    {
        $this->tgl1 = $tgl1;
        $this->tgl2 = $tgl2;
    }

    public function view(): View
    {
        $detailorderpembelian = T_detailorderpembelian::join('t_orderpembelians','t_detailorderpembelians.t_orderpembelian_id','=','t_orderpembelians.id')->where('t_detailorderpembelians.t_orderpembelian_id','<>','0')->where('t_orderpembelians.statusapprovel',true)->whereBetween('t_orderpembelians.tanggal',[$this->tgl1,$this->tgl2])->get();
        $grandtotal = 0;
        $idorder = 0;
        $total = 0;
        
        return view('laporan.rekappembelian.excelexport', [
            'detailorderpembelian' => $detailorderpembelian,
            'tgl1' => $this->tgl1,
            'tgl2' => $this->tgl2,
            'grandtotal' => $grandtotal,
            'idorder' => $idorder,
            'total' => $total
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}
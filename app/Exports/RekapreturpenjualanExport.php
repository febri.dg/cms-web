<?php

namespace App\Exports;

use App\T_detailunitretur;

use App\T_detailreturpenjualan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class RekapreturpenjualanExport implements WithColumnFormatting, FromView
{
    public $tgl1;
    public $tgl2;
    public $nomerproduk;

    public function __construct($tgl1,$tgl2)
    {
        $this->tgl1 = $tgl1;
        $this->tgl2 = $tgl2;
    }

    public function view(): View
    {
        $detailreturpenjualan = T_detailreturpenjualan::join('t_returpenjualans','t_detailreturpenjualans.t_returpenjualan_id','=','t_returpenjualans.id')->where('t_detailreturpenjualans.t_returpenjualan_id','<>','0')->where('t_returpenjualans.statusapprovel',true)->whereBetween('t_returpenjualans.tanggal',[$this->tgl1,$this->tgl2])->orderBy('t_detailreturpenjualans.t_returpenjualan_id')->get();
        $detailcek = T_detailreturpenjualan::select('t_detailreturpenjualans.id','t_detailreturpenjualans.m_produk_id')->join('t_returpenjualans','t_detailreturpenjualans.t_returpenjualan_id','=','t_returpenjualans.id')->where('t_detailreturpenjualans.t_returpenjualan_id','<>','0')->where('t_returpenjualans.statusapprovel',true)->whereBetween('t_returpenjualans.tanggal',[$this->tgl1,$this->tgl2])->orderBy('t_detailreturpenjualans.t_returpenjualan_id')->get();
        $idreturpenjualan = 0;

        foreach ($detailcek as $dk) {
            $produkunit = T_detailunitretur::where('m_produk_id',$dk->m_produk_id)->where('t_detailreturpenjualan_id',$dk->id)->where('t_unitretur_id','<>','0')->get();

            $nomerorder = '';
            $nomerserial = '';
            $n = 0;
            foreach ($produkunit as $pu) {
                if ($n==0) {
                    $nomerorder = $pu->nomer_order;
                    $nomerserial = $pu->nomer_serial;
                }else{
                    $nomerorder .= ', '.$pu->nomer_order;
                    $nomerserial .= ', '.$pu->nomer_serial;
                }
                $n++;
            }

            $this->nomerproduk[$dk->m_produk_id] = array(
                'nomerorder' => $nomerorder,
                'nomerserial' => $nomerserial
            );
        }
        
        return view('laporan.rekapreturpenjualan.excelexport', [
            'detailreturpenjualan' => $detailreturpenjualan,
            'tgl1' => $this->tgl1,
            'tgl2' => $this->tgl2,
            'idreturpenjualan' => $idreturpenjualan,
            'nomerproduk' => $this->nomerproduk
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}

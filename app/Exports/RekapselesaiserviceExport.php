<?php

namespace App\Exports;

use App\T_detailselesaiservice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RekapselesaiserviceExport implements FromView
{
    public $tgl1;
    public $tgl2;

    public function __construct($tgl1,$tgl2)
    {
        $this->tgl1 = $tgl1;
        $this->tgl2 = $tgl2;
    }

    public function view(): View
    {
        $detailselesaiservice = T_detailselesaiservice::join('t_selesaiservices','t_detailselesaiservices.t_selesaiservice_id','=','t_selesaiservices.id')->where('t_detailselesaiservices.t_selesaiservice_id','<>','0')->whereBetween('t_selesaiservices.tanggal',[$this->tgl1,$this->tgl2])->orderBy('t_detailselesaiservices.t_selesaiservice_id')->get();

        return view('laporan.rekapselesaiservice.excelexport', [
            'detailselesaiservice' => $detailselesaiservice,
            'tgl1' => $this->tgl1,
            'tgl2' => $this->tgl2
        ]);
    }
}
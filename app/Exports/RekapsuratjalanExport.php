<?php

namespace App\Exports;

use App\T_datapengirim;

use App\T_detailunitkeluar;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RekapsuratjalanExport implements FromView
{
    public $tgl1;
    public $tgl2;
    public $pengirim;

    public function __construct($tgl1,$tgl2)
    {
        $this->tgl1 = $tgl1;
        $this->tgl2 = $tgl2;
    }

    public function view(): View
    {
        $detailunitkeluar = T_detailunitkeluar::join('t_unitkeluars','t_detailunitkeluars.t_unitkeluar_id','=','t_unitkeluars.id')->where('t_detailunitkeluars.t_unitkeluar_id','<>','0')->whereBetween('t_unitkeluars.tanggal',[$this->tgl1,$this->tgl2])->orderBy('t_detailunitkeluars.t_unitkeluar_id')->get();
        $idunitkeluar = 0;

        foreach ($detailunitkeluar as $dk) {
            $datapengirim = T_datapengirim::where('t_unitkeluar_id',$dk->t_unitkeluar_id)->first();

            if(isset($datapengirim->id)){
                $this->pengirim[$dk->t_unitkeluar_id] = array(
                    'tgl_kirim' => $datapengirim->tgl_kirim,
                    'ekspedisi' => $datapengirim->ekspedisi,
                    'no_resi' => $datapengirim->no_resi,
                    'pengirim' => $datapengirim->pengirim,
                    'nopol' => $datapengirim->nopol,
                    'keterangan' => $datapengirim->keterangan
                );
            }else{
                $this->pengirim[$dk->t_unitkeluar_id] = array(
                    'tgl_kirim' => '',
                    'ekspedisi' => '',
                    'no_resi' => '',
                    'pengirim' => '',
                    'nopol' => '',
                    'keterangan' => ''
                );
            }
            
        }
        
        return view('laporan.suratjalan.excelexport', [
            'detailunitkeluar' => $detailunitkeluar,
            'tgl1' => $this->tgl1,
            'tgl2' => $this->tgl2,
            'idunitkeluar' => $idunitkeluar,
            'pengirim' => $this->pengirim
        ]);
    }
}

<?php

namespace App\Exports;

use App\T_detailreturpembelian;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RekapunitserviceExport implements FromView
{
    public $tgl1;
    public $tgl2;

    public function __construct($tgl1,$tgl2)
    {
        $this->tgl1 = $tgl1;
        $this->tgl2 = $tgl2;
    }

    public function view(): View
    {
        $detailreturpembelian = T_detailreturpembelian::join('t_returpembelians','t_detailreturpembelians.t_returpembelian_id','=','t_returpembelians.id')->where('t_detailreturpembelians.t_returpembelian_id','<>','0')->whereBetween('t_returpembelians.tanggal',[$this->tgl1,$this->tgl2])->orderBy('t_detailreturpembelians.t_returpembelian_id')->get();

        return view('laporan.rekapunitservice.excelexport', [
            'detailreturpembelian' => $detailreturpembelian,
            'tgl1' => $this->tgl1,
            'tgl2' => $this->tgl2
        ]);
    }
}
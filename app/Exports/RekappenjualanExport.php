<?php

namespace App\Exports;

use App\T_detailorderpenjualan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class RekappenjualanExport implements WithColumnFormatting, FromView
{
    public $tgl1;
    public $tgl2;

    public function __construct($tgl1,$tgl2)
    {
        $this->tgl1 = $tgl1;
        $this->tgl2 = $tgl2;
    }

    public function view(): View
    {
        $detailorderpenjualan = T_detailorderpenjualan::join('t_orderpenjualans','t_detailorderpenjualans.t_orderpenjualan_id','=','t_orderpenjualans.id')->where('t_detailorderpenjualans.t_orderpenjualan_id','<>','0')->where('t_orderpenjualans.statusapprovel',true)->whereBetween('t_orderpenjualans.tanggal',[$this->tgl1,$this->tgl2])->orderBy('t_detailorderpenjualans.t_orderpenjualan_id')->get();
        $idpenjualan = 0;
        $grandtotal = 0;
        $total = 0;
        
        return view('laporan.rekappenjualan.excelexport', [
            'detailorderpenjualan' => $detailorderpenjualan,
            'tgl1' => $this->tgl1,
            'tgl2' => $this->tgl2,
            'idpenjualan' => $idpenjualan,
            'grandtotal' => $grandtotal,
            'total' => $total
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }
}
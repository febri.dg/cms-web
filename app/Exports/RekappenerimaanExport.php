<?php

namespace App\Exports;

use App\T_detailpenerimaanbarang;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RekappenerimaanExport implements FromView
{
    public $tgl1;
    public $tgl2;

    public function __construct($tgl1,$tgl2)
    {
        $this->tgl1 = $tgl1;
        $this->tgl2 = $tgl2;
    }

    public function view(): View
    {
        $detailpenerimaanbarang = T_detailpenerimaanbarang::join('t_penerimaanbarangs','t_detailpenerimaanbarangs.t_penerimaanbarang_id','=','t_penerimaanbarangs.id')->where('t_detailpenerimaanbarangs.t_penerimaanbarang_id','<>','0')->whereBetween('t_penerimaanbarangs.tanggal',[$this->tgl1,$this->tgl2])->orderBy('t_detailpenerimaanbarangs.t_penerimaanbarang_id')->orderBy('t_detailpenerimaanbarangs.part_id')->get();
        $partidcek = '';
        $idpenerimaan = 0;

        return view('laporan.rekappenerimaan.excelexport', [
            'detailpenerimaanbarang' => $detailpenerimaanbarang,
            'tgl1' => $this->tgl1,
            'tgl2' => $this->tgl2,
            'partidcek' => $partidcek,
            'idpenerimaan' => $idpenerimaan
        ]);
    }
}
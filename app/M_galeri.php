<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_galeri extends Model
{
    protected $fillable = ['m_album_id','keterangan','gambar','key','tgl_post','post'];

    public function m_albums()
    {
        return $this->belongsTo('App\M_album','m_album_id');
    }
}

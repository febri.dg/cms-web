<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_websubmenu extends Model
{
    public function m_webmenus()
    {
        return $this->belongsTo('App\M_webmenu');
    }
}

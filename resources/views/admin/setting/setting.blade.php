@extends('temp')

@section('main')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Web Setting</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Web Setting </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="formtambah" method="post" action="{{ env('APP_URL') }}/admin/aksieditsetting"
                data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Website
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="nama" name="nama" class="form-control col-md-7 col-xs-12" value="{{ $nama->nilai }}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="alamat" name="alamat" class="form-control col-md-7 col-xs-12" value="{{ $alamat->nilai }}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kota
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="kota" name="kota" class="form-control col-md-7 col-xs-12" value="{{ $kota->nilai }}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Telepon
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="telp" name="telp" class="form-control col-md-7 col-xs-12" value="{{ $telp->nilai }}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="email" name="email" class="form-control col-md-7 col-xs-12" value="{{ $email->nilai }}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Profil
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea name="profil" id="posttext" style="width:100%;height:300px" >{{ $profil->isi }}</textarea>
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="{{ env('APP_URL') }}/admin/setting"><button class="btn btn-primary" type="reset">Reset</button></a>
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

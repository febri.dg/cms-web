@extends('temp')

@section('main')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Otoritas</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>List Otoritas </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#listotoritas" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Otoritas</a>
                </li>
                <li role="presentation" class=""><a href="#detailotoritas" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Detail Otoritas</a>
                </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="listotoritas" aria-labelledby="home-tab">
                  <table id="datatable-fixed-header" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Nama Otoritas</th>
                        <th>Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach ($otoriti as $o)
                        <tr>
                          <td>{{ $o->nama }}</td>
                          <td>
                            <a href="{{ env('APP_URL') }}/admin/otoritas/editotoritas/{{ $o->id }}">
                            <button type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button></a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="{{ env('APP_URL') }}/admin/otoritas/tambahotoritas"><button class="btn btn-primary" type="reset"><i class="fa fa-plus"></i> Tambah Otoritas</button></a>
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="detailotoritas" aria-labelledby="profile-tab">
                  <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Nama Otoritas</th>
                        <th>Menu</th>
                        <th>Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach ($detailotoriti as $do)
                        <tr>
                          <td>{{ $do->m_otoritis->nama }}</td>
                          <td>
                            <ul>
                                @foreach(OtoritiHelper::get_menu($do->m_otoriti_id) as
                                $domn)
                                <li>{{ $domn->m_menus->nama }}</li>
                                @foreach(OtoritiHelper::get_submenu($domn->m_menu_id,$do->m_otoriti_id)
                                as $dosmn)
                                <li>--- {{ $dosmn->m_submenus->nama }}</li>
                                @endforeach
                                @endforeach
                            </ul>
                          </td>
                          <td>
                            <a href="{{ env('APP_URL') }}/admin/otoritas/editdetailotoritas/{{ $do->m_otoriti_id }}">
                            <button type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button></a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <a href="{{ env('APP_URL') }}/admin/otoritas/tambahdetailotoritas"><button class="btn btn-primary" type="reset"><i class="fa fa-plus"></i> Tambah Detail Otoritas</button></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

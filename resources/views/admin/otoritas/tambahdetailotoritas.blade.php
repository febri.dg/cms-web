@extends('temp')

@section('main')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Otoritas</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tambah Detail Otoritas </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="formtambah" method="post" action="{{ env('APP_URL') }}/admin/otoritas/aksitambahdetailotoritas"
                data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
              {{ csrf_field() }}
              @if (session('statusdetailotoritas'))
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                {{ session('statusdetailotoritas') }}
              </div>
              @endif
              <div class="form-group">
                <label for="first-name">Nama Otoritas</label>
                <select class="form-control" name="otoritas" id="otoritas">
                  <<option value="0">-- Pilih Otoritas --</option>
                  @foreach($otoritas as $o)
                  @if(OtoritiHelper::cekotoriti($o->id)!=1)
                  <option value="{{ $o->id }}">{{ $o->nama }}</option>
                  @endif
                  @endforeach
                </select>

              </div>
              <div class="form-group">
                <label for="first-name">Daftar Menu</label>
                <div class="table-responsive">
                  <table class="table table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th>
                          <input type="checkbox" id="check-all" class="flat">
                        </th>
                        <th scope="col"></th>
                        <th scope="col">Nama Menu</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($menu as $mn)
                      <tr class="even pointer">
                          <td></td>
                          <td>{{ $mn->nama }}</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                      </tr>
                      @foreach($mn->m_submenus as $smn)
                      <tr class="even pointer">
                          <td class="a-center "><input type="checkbox" class="flat" name="sm{{ $mn->id }}{{ $smn->id }}"
                                  value=" {{ $smn->id }}"></td>
                          <td>--- {{ $smn->nama }}</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                      </tr>
                      @endforeach
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                  <a href="{{ env('APP_URL') }}/admin/otoritas"><button class="btn btn-info" type="button"><i class="fa fa-reply"></i> Kembali</button></a>
                  <a href="{{ env('APP_URL') }}/admin/otoritas/tambahdetailotoritas"><button class="btn btn-primary" type="reset"><i class="fa fa-refresh"></i> Reset</button></a>
                  <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Submit</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

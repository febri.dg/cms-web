@extends('temp')

@section('main')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>User</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tambah User </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="formtambah" method="post" action="{{ env('APP_URL') }}/admin/user/aksitambahuser"
                data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
              {{ csrf_field() }}
              @if (session('statususer'))
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                {{ session('statususer') }}
              </div>
              @endif
              <div class="form-group">
                <label for="first-name">Nama User</label>
                <input type="text" id="nama" name="nama" class="form-control">
              </div>
              <div class="form-group">
                <label for="first-name">Username</label>
                <input type="text" id="username" name="username" class="form-control">
              </div>
              <div class="form-group">
                <label for="first-name">Password</label>
                <input type="password" id="password1" name="password1" class="form-control">
              </div>
              <div class="form-group">
                <label for="first-name">Ulangi Password</label>
                <input type="password" id="password2" name="password2" class="form-control">
              </div>
              <div class="form-group">
                <label for="first-name">Nama Otoritas</label>
                <select class="form-control" name="otoritas" id="otoritas">
                  <<option value="0">-- Pilih Otoritas --</option>
                  @foreach($otoriti as $o)
                  <option value="{{ $o->id }}">{{ $o->nama }}</option>
                  @endforeach
                </select>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                  <a href="{{ env('APP_URL') }}/admin/user"><button class="btn btn-info" type="button"><i class="fa fa-reply"></i> Kembali</button></a>
                  <a href="{{ env('APP_URL') }}/admin/user/tambahuser"><button class="btn btn-primary" type="reset"><i class="fa fa-refresh"></i> Reset</button></a>
                  <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Submit</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

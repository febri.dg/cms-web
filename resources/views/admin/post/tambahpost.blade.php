@extends('temp')

@section('main')
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Post</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <form id="formtambah" method="post" action="{{ env('APP_URL') }}/admin/post/aksitambahpost"
          data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if (session('statuspost'))
        <div class="alert alert-success alert-dismissible fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          {{ session('statuspost') }}
        </div>
        @endif
        <div class="col-md-9 col-sm-9 col-xs-9">
          <div class="x_panel">
            <div class="x_title">
              <h2>Tambah Post </h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="form-group">
                  <label for="first-name">Judul</label>
                  <input type="text" id="judul" name="judul" class="form-control">
                </div>
                <div class="form-group">
                  <label for="first-name">Kategori</label>
                  <select class="form-control" name="kategori" id="kategori">
                    <<option value="0">-- Pilih Kategori --</option>
                    @foreach($kategoripost as $k)
                    <option value="{{ $k->id }}">{{ $k->nama }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="first-name">Isi</label>
                  <textarea name="isi" id="posttext" style="width:100%;height:600px" ></textarea>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <a href="{{ env('APP_URL') }}/admin/post"><button class="btn btn-info" type="button"><i class="fa fa-reply"></i> Kembali</button></a>
                    <a href="{{ env('APP_URL') }}/admin/post/tambahpost"><button class="btn btn-primary" type="reset"><i class="fa fa-refresh"></i> Reset</button></a>
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Submit</button>
                  </div>
                </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
          <div class="x_panel">
            <div class="x_title">
              <h2>Atribut </h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="form-group">
                  <label for="first-name">Aktif</label><br />
                  <div id="aktif" class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="aktif" value="N" checked> &nbsp; Tidak Aktif &nbsp;
                    </label>
                    <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="aktif" value="Y"> Aktif
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="first-name">Hamopage</label><br />
                  <div id="aktif" class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="homepage" value="N"> &nbsp; Tidak &nbsp;
                    </label>
                    <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="homepage" value="Y" checked> Ya
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="first-name">Meta Key</label><br />
                  <input id="tags_1" type="text" name="key" class="tags form-control" />
                </div>
                <div class="form-group">
                  <label for="first-name">Gambar</label><br />
                  <input type="file" name="gambar" class="tags form-control" />
                </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@stop

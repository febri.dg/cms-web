@extends('temp')

@section('main')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Ganti Password</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Utility</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Pegawai</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Ganti Password</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        @if (session('statusgantipassword'))
                        <div class="card-sub">
                            {{ session('statusgantipassword') }}
                        </div>
                        @endif
                        <form method="post" action="{{ env('APP_URL') }}/gantipassword/aksigantipassword/{{ $iduser }}"
                            enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card-header">
                                <h4 class="card-title">Ganti Password</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 col-lg-12">
                                        <div class="form-group">
                                            <label for="email2">Password Baru </label>
                                            <input type="password" class="form-control" name="password1" id="password1"
                                                placeholder="Password ">
                                        </div>
                                        <div class="form-group">
                                            <label for="email2">Ulangi Password </label>
                                            <input type="password" class="form-control" name="password2" id="password2"
                                                placeholder="Ulangi Password ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-action">
                                <button type="submit" class="btn btn-secondary">
                                    <span class="btn-label">
                                        <i class="fa fa-save"></i>
                                    </span>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @stop
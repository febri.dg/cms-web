@extends('temp')

@section('main')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <!--<h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                        <h5 class="text-white op-7 mb-2">Free Bootstrap 4 Admin Dashboard</h5>-->
                    </div>
                    <div class="ml-md-auto py-2 py-md-0">
                        <!--<a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a>
                        <a href="#" class="btn btn-secondary btn-round">Add Customer</a>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row mt--2">
                <div class="col-md-6">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">DATA PERMINTAAN PENGADAAN</div>
                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="pengajuan"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH PENGAJUAN</h6>
                                </div>
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="pengajuanapprovel"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH APPROVEL</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">DATA PO</div>
                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="po"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH PENGAJUAN</h6>
                                </div>
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="poapprovel"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH APPROVEL</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">DATA PENERIMAAN BARANG</div>
                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_penerimaan"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">DATA UNIT SERVICE DAN SELESAI SERVICE</div>
                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_returbeli"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH UNIT SERVICE</h6>
                                </div>
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_selesaiservice"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH SELESAI SERVICE</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">DATA PENJUALAN</div>
                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_penjualan"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH PENGAJUAN</h6>
                                </div>
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_penjualanapprovel"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH APPROVEL</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">DATA UNIT KELUAR</div>
                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_unitkeluar"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">DATA RETUR PENJUALAN</div>
                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_returjual"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH PENGAJUAN</h6>
                                </div>
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_returjualapprovel"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH APPROVEL</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">DATA UNIT RETUR</div>
                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                <div class="px-2 pb-2 pb-md-0 text-center">
                                    <div id="jml_unitretur"></div>
                                    <h6 class="fw-bold mt-3 mb-0">JUMLAH</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.themekita.com">
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </footer>
</div>
    
@stop
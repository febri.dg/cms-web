@extends('tempweb')

@section('main')
<!-- ********** Hero Area Start ********** -->
<div class="hero-area">

    <!-- Hero Slides Area -->
    <div class="hero-slides owl-carousel">
        @foreach($postslide as $ps)
        <!-- Single Slide -->
        <div class="single-hero-slide bg-img background-overlay" style="background-image: url({{ env('APP_URL') }}/public/galeri/{{ $ps->gambar }});"></div>
        @endforeach
    </div>

    <!-- Hero Post Slide -->
    <div class="hero-post-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="hero-post-slide">
                      @php
                        $n = 1;
                      @endphp
                      @foreach($postslide as $ps2)
                        <!-- Single Slide -->
                        <div class="single-slide d-flex align-items-center">
                            <div class="post-number">
                                <p>{{ $n }}</p>
                            </div>
                            <div class="post-title">
                                <a href="{{ env('APP_URL') }}/post/{{ $ps2->tag }}">{{ $ps2->judul }}</a>
                            </div>
                        </div>
                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ********** Hero Area End ********** -->

<div class="main-content-wrapper section-padding-100">
    <div class="container">
      <div class="world-latest-articles">
          <div class="row">
              <div class="col-12 col-lg-8">
                  <div class="title">
                      <h5>Berita</h5>
                  </div>

                  @foreach($kategoriberita->m_postfronts as $pb)
                    @php
                      $postberita = substr(strip_tags($pb->isi), 0, 50);
                    @endphp
                    <!-- Single Blog Post -->
                    <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
                        <!-- Post Thumbnail -->
                        <div class="post-thumbnail">
                            <img src="{{ env('APP_URL') }}/public/galeri/{{ $pb->gambar }}" alt="">
                        </div>
                        <!-- Post Content -->
                        <div class="post-content">
                            <a href="{{ env('APP_URL') }}/post/{{ $pb->tag }}" class="headline">
                                <h5>{{ $pb->judul }}</h5>
                            </a>
                            <p>{{ $postberita }}...</p>
                        </div>
                    </div>
                  @endforeach
              </div>

              <div class="col-12 col-lg-4">
                <div class="post-sidebar-area wow fadeInUpBig" data-wow-delay="0.2s">
                    <!-- Widget Area -->
                    <div class="sidebar-widget-area">
                        <h5 class="title">Profil</h5>
                        <div class="widget-content">
                            @php
                              $isiprofil = substr(strip_tags($profil->isi), 0, 50);
                            @endphp
                            <p>{{ $isiprofil }}</p>
                        </div>
                    </div>
                    <!-- Widget Area -->
                    <div class="sidebar-widget-area">
                        <h5 class="title">Terbaru</h5>
                        <div class="widget-content">
                            @foreach($postterbaru as $ptr)
                              <!-- Single Blog Post -->
                              <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                  <!-- Post Thumbnail -->
                                  <div class="post-thumbnail">
                                      <img src="{{ env('APP_URL') }}/public/galeri/{{ $ptr->gambar }}" alt="">
                                  </div>
                                  <!-- Post Content -->
                                  <div class="post-content">
                                      <a href="{{ env('APP_URL') }}/post/{{ $ptr->tag }}" class="headline">
                                          <h5 class="mb-0">{{ $ptr->judul }}</h5>
                                      </a>
                                  </div>
                              </div>
                            @endforeach
                        </div>
                    </div>
                    
                </div>

              </div>
          </div>
      </div>

      <div class="world-latest-articles">
          <div class="row">
              <div class="col-12 col-lg-8">
                  <div class="title">
                      <h5>Artikel</h5>
                  </div>

                  @foreach($kategoriartikel->m_postfronts as $pa)
                    @php
                      $postartikel = substr(strip_tags($pa->isi), 0, 50);
                    @endphp
                    <!-- Single Blog Post -->
                    <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
                        <!-- Post Thumbnail -->
                        <div class="post-thumbnail">
                            <img src="{{ env('APP_URL') }}/public/galeri/{{ $pa->gambar }}" alt="">
                        </div>
                        <!-- Post Content -->
                        <div class="post-content">
                            <a href="{{ env('APP_URL') }}/post/{{ $pa->tag }}" class="headline">
                                <h5>{{ $pa->judul }}</h5>
                            </a>
                            <p>{{ $postartikel }}...</p>
                        </div>
                    </div>
                  @endforeach
              </div>
          </div>
      </div>

    </div>
</div>
@stop

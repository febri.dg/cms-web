@extends('tempweb')

@section('main')
<!-- ********** Hero Area Start ********** -->
<div class="hero-area height-600 bg-img background-overlay" style="background-image: url({{ env('APP_URL') }}/assets/images/profil.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div class="single-blog-title text-center">
                    <!-- Catagory -->
                    <div class="post-cta"></div>
                    <h3>PROFIL SMPN 3 NEGARA</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ********** Hero Area End ********** -->

<div class="main-content-wrapper section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area ============= -->
            <div class="col-12 col-lg-12">
                <div class="single-blog-content mb-100">
                    <!-- Post Content -->
                    <div class="post-content">
                        {!! $profil->isi !!}
                    </div>
                </div>
            </div>
          </div>
    </div>
</div>
@stop

@extends('tempweb')

@section('main')
<!-- ********** Hero Area Start ********** -->
<div class="hero-area height-600 bg-img background-overlay" style="background-image: url({{ env('APP_URL') }}/public/galeri/{{ $post->gambar }});">
  <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center">
          <div class="col-12 col-md-8 col-lg-6">
              <div class="single-blog-title text-center">
                  <!-- Catagory -->
                  <div class="post-cta"></div>
                  <h3>{{ $post->judul }}</h3>
              </div>
          </div>
      </div>
  </div>
</div>
<!-- ********** Hero Area End ********** -->

<div class="main-content-wrapper section-padding-100">
    <div class="container">
      <div class="row justify-content-center">
          <!-- ============= Post Content Area ============= -->
          <div class="col-12 col-lg-8">
            <div class="single-blog-content mb-100">
                <!-- Post Content -->
                <div class="post-content">
                    {{ $post->isi }}
                </div>
            </div>
          </div>

          <!-- ========== Sidebar Area ========== -->
          <div class="col-12 col-md-8 col-lg-4">
            <div class="post-sidebar-area wow fadeInUpBig" data-wow-delay="0.2s">
                <!-- Widget Area -->
                <div class="sidebar-widget-area">
                    <h5 class="title">Profil</h5>
                    <div class="widget-content">
                        @php
                          $isiprofil = substr(strip_tags($profil->isi), 0, 50);
                        @endphp
                        <p>{{ $isiprofil }}</p>
                    </div>
                </div>
                <!-- Widget Area -->
                <div class="sidebar-widget-area">
                    <h5 class="title">Terbaru</h5>
                    <div class="widget-content">
                        @foreach($postterbaru as $ptr)
                          <!-- Single Blog Post -->
                          <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                              <!-- Post Thumbnail -->
                              <div class="post-thumbnail">
                                  <img src="{{ env('APP_URL') }}/public/galeri/{{ $ptr->gambar }}" alt="">
                              </div>
                              <!-- Post Content -->
                              <div class="post-content">
                                  <a href="{{ env('APP_URL') }}/post/{{ $ptr->tag }}" class="headline">
                                      <h5 class="mb-0">{{ $ptr->judul }}</h5>
                                  </a>
                              </div>
                          </div>
                        @endforeach
                    </div>
                </div>
            </div>
          </div>
      </div>
    </div>
</div>
@stop

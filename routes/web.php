<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', 'viewadminhome@home');
Route::get('/', 'viewhome@home');
Route::get('/profil', 'viewprofil@profil');
Route::get('/berita', 'viewberita@berita');
Route::get('/artikel', 'viewartikel@artikel');
Route::get('/kontak', 'viewkontak@kontak');
Route::get('/post/{tag}', 'viewwebpost@post');
//Route::get('/tes', 'viewhome@tes');
//login
Route::post('/admin/proses/login','aksilogin@login');
Route::get('/admin/proses/logout','aksilogin@logout');
Route::get('/proses/modul/{id}','aksilogin@modul');

//ganti password
Route::get('/gantipassword','aksilogin@gantipassword');
Route::post('/gantipassword/aksigantipassword/{id}','aksilogin@aksigantipassword');

//Setting
Route::get('/admin/setting','viewsetting@setting');
Route::post('/admin/aksieditsetting','viewsetting@aksieditsetting');

//kategori post
Route::get('/admin/album', 'viewalbum@listalbum');
Route::get('/admin/album/tambahalbum', 'viewalbum@tambahalbum');
Route::post('/admin/album/aksitambahalbum', 'viewalbum@aksitambahalbum');
Route::get('/admin/album/editalbum/{id}','viewalbum@editalbum');
Route::post('/admin/album/aksieditalbum/{id}', 'viewalbum@aksieditalbum');

//kategori post
Route::get('/admin/kategoripost', 'viewkategori@listkategori');
Route::get('/admin/kategoripost/tambahkategori', 'viewkategori@tambahkategori');
Route::post('/admin/kategoripost/aksitambahkategori', 'viewkategori@aksitambahkategori');
Route::get('/admin/kategoripost/editkategori/{id}','viewkategori@editkategori');
Route::post('/admin/kategoripost/aksieditkategori/{id}', 'viewkategori@aksieditkategori');

//otoritas
Route::get('/admin/otoritas', 'viewotoritas@listotoritas');
Route::get('/admin/otoritas/tambahotoritas', 'viewotoritas@tambahotoritas');
Route::post('/admin/otoritas/aksitambahotoritas', 'viewotoritas@aksitambahotoritas');
Route::get('/admin/otoritas/editotoritas/{id}','viewotoritas@editotoritas');
Route::post('/admin/otoritas/aksieditotoritas/{id}', 'viewotoritas@aksieditotoritas');
Route::get('/admin/otoritas/tambahdetailotoritas', 'viewotoritas@tambahdetailotoritas');
Route::get('/admin/otoritas/menutambahdetailotoritas/{id}', 'viewotoritas@menutambahdetailotoritas');
Route::post('/admin/otoritas/aksitambahdetailotoritas', 'viewotoritas@aksitambahdetailotoritas');
Route::get('/admin/otoritas/editdetailotoritas/{id}','viewotoritas@editdetailotoritas');
Route::post('/admin/otoritas/aksieditdetailotoritas/{id}', 'viewotoritas@aksieditdetailotoritas');
Route::get('/admin/otoritas/deleteotoritas/{id}', 'viewotoritas@deleteotoritas');
Route::get('/admin/otoritas/deletedetailotoritas/{id}', 'viewotoritas@deletedetailotoritas');

//pegawai
Route::get('/admin/user', 'viewuser@listuser');
Route::get('/admin/user/tambahuser', 'viewuser@tambahuser');
Route::post('/admin/user/aksitambahuser', 'viewuser@aksitambahuser');
Route::get('/admin/user/edituser/{id}','viewuser@edituser');
Route::post('/admin/user/aksiedituser/{id}', 'viewuser@aksiedituser');
Route::get('/admin/user/deleteuser/{id}', 'viewuser@deleteuser');

//post
Route::get('/admin/post', 'viewpost@listpost');
Route::get('/admin/post/tambahpost', 'viewpost@tambahpost');
Route::post('/admin/post/aksitambahpost', 'viewpost@aksitambahpost');
Route::get('/admin/post/editpost/{id}','viewpost@editpost');
Route::post('/admin/post/aksieditpost/{id}', 'viewpost@aksieditpost');
Route::get('/admin/post/deletepost/{id}', 'viewpost@deletepost');

Route::auth();
